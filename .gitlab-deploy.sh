#!/bin/bash
set -f
echo "$SSH_PRIVATE_KEY" > aws.pem
chmod 600 aws.pem
mkdir ~/.ssh
ssh-keyscan $DEPLOY_SERVER >> ~/.ssh/known_hosts

mvn package

ssh -i aws.pem ubuntu@$DEPLOY_SERVER "sudo systemctl stop $SERVICE_NAME"

chmod 777 target/techexercise.jar

scp -i aws.pem target/techexercise.jar ubuntu@$DEPLOY_SERVER:/var/www/java
ssh -i aws.pem ubuntu@$DEPLOY_SERVER "sudo systemctl start $SERVICE_NAME"