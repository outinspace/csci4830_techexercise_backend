package space.outin.softwareEngineering.techExercise;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("")
public class TodoController {

    @Autowired
    private TodoRepository repo;

    @PostMapping("/apply")
    public void apply(@RequestBody Todo td) {
        repo.save(td);
    }

    @GetMapping("/list")
    public List<Todo> list(@RequestParam(value="label", required=false) String label) {
        if (label != null && !label.isEmpty()) {
            return repo.findByLabelContainingIgnoreCase(label);
        } else {
            return repo.findAll();
        }
    }
    
    @GetMapping("/delete")
    public void delete(@RequestParam("id") Integer id) {
        repo.deleteById(id);
    }
}
