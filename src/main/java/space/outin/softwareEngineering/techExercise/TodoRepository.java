package space.outin.softwareEngineering.techExercise;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface TodoRepository extends JpaRepository<Todo, Integer> {

    public List<Todo> findByLabelContainingIgnoreCase(@Param("label") String label);
}