package space.outin.softwareEngineering.techExercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechExerciseApplication.class, args);
	}

}

